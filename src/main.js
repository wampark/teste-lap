import Vue from 'vue'
import App from './App.vue'
import store from './store/index'
import router from './router/router'
import jQuery from 'jquery'
import $ from 'jquery'
import Popper from 'popper.js'
import 'bootstrap'
import './assets/scss/_base.scss'



new Vue({
  el: '#app',
  render: h => h(App),
  store,
  router
})
