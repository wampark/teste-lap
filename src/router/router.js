import Vue from 'vue'
import Router from 'vue-router'
import Home from './../components/templates/Home'

Vue.use(Router);

const router = new Router({
    routes: [
      {
        path: '*',
        redirect: '/'
      },
      {
        path: '/',
        name: 'home',
        component: Home
      }
    ],
    mode: 'history',
    linkActiveClass: 'active'
})

export default router