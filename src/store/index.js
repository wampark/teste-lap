import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    data: []
  },
  mutations: {
    setData(state, value) {
      return (state.data = value);
    },
    getData(state) {
      return state.data;
    }
  }
});

export default store;
